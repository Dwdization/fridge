package cz.dhudec.fridge.repository;

import cz.dhudec.fridge.domain.Food;
import cz.dhudec.fridge.domain.FoodType;
import cz.dhudec.fridge.domain.Household;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface FoodRepository extends PagingAndSortingRepository<Food, Long> {

    @Query(name = "allFoodsForHousehold",
           value = "select f from Food f join f.type ft join ft.household h where h = :household order by f.id")
    List<Food> findAllForHousehold(@Param("household") Household household);

    @Query(name = "allFoodsForHousehold",
            value = "select f from Food f join f.type ft join ft.household h where h = :household order by f.id")
    Page<Food> findAllForHousehold(@Param("household") Household household, Pageable pageable);

    @Query(name = "allFoodsForHouseholdByEaten",
           value = "select f from Food f join f.type ft join ft.household h where h = :household and ((true = :eaten and f.eaten > 0) or (false = :eaten and f.quantity > f.eaten)) order by f.id")
    List<Food> findAllByEatenForHousehold(@Param("household") Household household, @Param("eaten") boolean eaten);

    @Query(name = "allFoodsForHouseholdByEaten",
            value = "select f from Food f join f.type ft join ft.household h where h = :household and ((true = :eaten and f.eaten > 0) or (false = :eaten and f.quantity > f.eaten)) order by f.id")
    Page<Food> findAllByEatenForHousehold(@Param("household") Household household, @Param("eaten") boolean eaten, Pageable pageable);

    @Query(name = "foodsNeedingNotification",
           value = "select f from Food f join f.type ft join ft.household h where h = :household and f.eaten < f.quantity and (f.expires - f.notificationPeriod)  <= CURRENT_DATE")
    List<Food> findFoodsThatNeedNotificationForHousehold(@Param("household") Household household);

    boolean existsByType(FoodType foodType);

    @Query(name = "uniqueFoodNames",
           value = "select distinct f.name from Food f")
    List<String> getUniqueNames();
}
