package cz.dhudec.fridge.repository;

import cz.dhudec.fridge.domain.Household;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HouseholdRepository extends JpaRepository<Household, Long> {
    Household findOneByLogin(String login);
}
