package cz.dhudec.fridge.repository;

import cz.dhudec.fridge.domain.FoodType;
import cz.dhudec.fridge.domain.Household;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface FoodTypeRepository extends JpaRepository<FoodType, Long> {

    List<FoodType> findAllByHouseholdOrderById(@Param("household") Household household);
}
