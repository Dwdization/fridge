package cz.dhudec.fridge.domain;

import cz.dhudec.fridge.service.api.NotificationService;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Entity
@ToString
@EqualsAndHashCode
@Table(name = "email")
public class Email implements NotificationService.Notification.Addressee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @NotEmpty
    private String address;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "household_id", nullable = false)
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Household household;
}
