package cz.dhudec.fridge.domain;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "food")
public class Food {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "food_type_id")
    private FoodType type;

    @NotEmpty(message = "Name is mandatory")
    private String name;

    @Enumerated(EnumType.ORDINAL)
    @NotNull
    private Placement placement;

    @Min(value = 1, message = "The value must be at least 1")
    private int quantity;

    @Min(value = 0, message = "The value must be at least 1")
    private int eaten;

    @Nullable
    @Min(value = 1, message = "The value must be positive")
    private int notificationPeriod;

    @NotNull(message = "Expiry date is mandatory")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate expires;

    @NotNull(message = "Creation date is mandatory")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate created;

    @Transient
    public boolean hasAnyLeft() {
        return quantity > eaten;
    }

    @Transient
    public int getNumberOfReady() {
        return quantity - eaten;
    }
}
