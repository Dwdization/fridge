package cz.dhudec.fridge.domain;

public enum Placement {
    FRIDGE("placement.fridge"),
    FREEZER("placement.freezer");

    private String translationKey;

    Placement(String translationKey) {
        this.translationKey = translationKey;
    }

    public String getTranslationKey() {
        return translationKey;
    }
}
