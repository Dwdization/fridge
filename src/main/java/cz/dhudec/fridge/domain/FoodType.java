package cz.dhudec.fridge.domain;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "food_type")
public class FoodType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty(message = "Name is mandatory")
    private String name;

    @NotNull(message = "The value must not be empty")
    @Min(value = 0, message = "The value must be positive")
    private Integer defaultDurability = 2;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "household_id")
    private Household household;

    @NotNull(message = "Creation date is mandatory")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate created;

    // TODO(D. Hudec): 30.07.19 doplnit property 'icon'
    //  pokud nedostanu font awesome pro, nema to asi smysl
}
