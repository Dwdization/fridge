package cz.dhudec.fridge.domain;

import lombok.Data;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Set;

@Data
@ToString
@Entity
@Table(name = "household")
public class Household {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty(message = "Login is mandatory")
    private String login;

    @NotEmpty(message = "Name is mandatory")
    private String name;

    @OneToMany(mappedBy = "household", fetch = FetchType.EAGER)
    @NotNull
    private Set<Email> emails;

    @NotNull(message = "Creation date is mandatory")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate created;
}
