package cz.dhudec.fridge;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .permitAll()
                .and()
                .logout()
                .permitAll();
    }

    @Bean
    @Override
    public UserDetailsService userDetailsService() {
        UserDetails hudcovicLednicka = User
                .withUsername("hudcovic_lednicka")
                .password("{bcrypt}$2a$10$/sucqQFOEnugKcdsCH6DqugaqMITrbuDZzs.8WpmHk3Zf7x8gBn0e")
                .roles("USER")
                .build();

        UserDetails janouskovicLednicka = User
                .withUsername("janouskovic_lednicka")
                .password("{bcrypt}$2a$10$tB0I0AwdSMd1Hqp1uOSEBeX0fQh2BdlA6.MzU693G5VRtlxHfcbwK")
                .roles("USER")
                .build();

        return new InMemoryUserDetailsManager(hudcovicLednicka, janouskovicLednicka);
    }
}
