package cz.dhudec.fridge.dto;

import cz.dhudec.fridge.domain.Food;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FoodNotificationDto {

    private String name;
    private int pieces;
    private boolean alreadyExpired;
    private LocalDate expiryDate;

    public static FoodNotificationDto from(Food food) {
        FoodNotificationDto dto = new FoodNotificationDto();

        LocalDate expiryDate = food.getExpires();

        dto.name = food.getName();
        dto.pieces = food.getNumberOfReady();
        dto.alreadyExpired = expiryDate.isBefore(LocalDate.now());
        dto.expiryDate = expiryDate;

        return dto;
    }
}
