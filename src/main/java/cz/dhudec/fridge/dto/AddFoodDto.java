package cz.dhudec.fridge.dto;

import cz.dhudec.fridge.domain.Food;
import cz.dhudec.fridge.domain.FoodType;
import cz.dhudec.fridge.domain.Placement;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.lang.Nullable;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Objects;

@Data
public class AddFoodDto {

    @NotNull(message = "Food type is mandatory")
    private FoodType type;

    @NotEmpty(message = "Name is mandatory")
    private String name;

    @Min(value = 1, message = "The value must be at least 1")
    private int quantity;

    @NotNull(message = "Placement is mandatory")
    private Placement placement;

    @Nullable
    @Min(value = 1, message = "The value must be positive")
    private int notificationPeriod;

    @NotNull(message = "Expiry date is mandatory")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate expires;

    @NotNull(message = "Creation date is mandatory")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate created;

    public Food toFood() {
        Food food = new Food();

        food.setName(name);
        food.setExpires(expires);
        food.setType(type);
        food.setPlacement(placement);
        food.setCreated(created);
        food.setQuantity(quantity);
        food.setNotificationPeriod(notificationPeriod);

        return food;
    }
}
