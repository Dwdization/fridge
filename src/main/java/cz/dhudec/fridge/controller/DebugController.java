package cz.dhudec.fridge.controller;

import cz.dhudec.fridge.domain.Email;
import cz.dhudec.fridge.domain.Household;
import cz.dhudec.fridge.service.HouseholdHolder;
import cz.dhudec.fridge.service.NotificationCreator;
import cz.dhudec.fridge.service.api.NotificationService;
import cz.dhudec.fridge.service.api.NotificationService.Notification.Addressee;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.stream.Collectors;

@Controller
@RequestMapping(value = "/debug")
@Slf4j
public class DebugController {

    private final NotificationService notificationService;
    private final NotificationCreator notificationCreator;
    private final HouseholdHolder householdHolder;

    public DebugController(NotificationService notificationService, NotificationCreator notificationCreator, HouseholdHolder householdHolder) {
        this.notificationService = notificationService;
        this.notificationCreator = notificationCreator;
        this.householdHolder = householdHolder;
    }

    @RequestMapping(path = "mail/send", method = RequestMethod.GET)
    public String sendMail() {
        notificationService.sendAdvancedNotification(notificationCreator.apply(householdHolder.get()));

        return "redirect:/foods";
    }

    @ResponseBody
    @RequestMapping(path = "mail/show", method = RequestMethod.GET)
    public String showMail() {
        NotificationService.Notification notification = notificationCreator.apply(householdHolder.get());

        return "receivers: " + notification.getReceivers().stream().map(Addressee::getAddress).collect(Collectors.joining(", "))
                + "</br>subject: " + notification.getHeader()
                + "</br>---------</br>" + notification.getBody()
                + "</br>---------</br><a href=\"/debug/mail/send\">send</a>&nbsp<a href=\"/\">home</a>";
    }

    @ResponseBody
    @RequestMapping(path = "logged", method = RequestMethod.GET)
    public String getLoggedUser() {
        Household logged = householdHolder.get();
        return String.format("%s (%s)", logged.getName(), logged.getLogin());
    }

    @ResponseBody
    @RequestMapping(path = "hash", method = RequestMethod.GET)
    public String triggerHashing(@RequestParam(name = "what") String toHash) {
        PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();

        return encoder.encode(toHash);
    }

    @RequestMapping(path = "wakeup", method = RequestMethod.GET)
    public String wakeUp(HttpServletRequest request) {
        log.info("Waking up by request from {}", request.getRemoteAddr());

        return "redirect:/foods";
    }
}
