package cz.dhudec.fridge.controller;

import cz.dhudec.fridge.domain.Food;
import cz.dhudec.fridge.domain.FoodType;
import cz.dhudec.fridge.domain.Placement;
import cz.dhudec.fridge.dto.AddFoodDto;
import cz.dhudec.fridge.service.api.FoodService;
import cz.dhudec.fridge.service.api.FoodTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@Controller
@RequestMapping(value = "/foods")
public class FoodController {

    private final FoodService foodService;
    private final FoodTypeService foodTypeService;

    @Autowired
    public FoodController(FoodService foodService, FoodTypeService foodTypeService) {
        this.foodService = foodService;
        this.foodTypeService = foodTypeService;
    }

    @GetMapping("")
    public String getAllReady(Model model, @PageableDefault Pageable pageable) {
        model.addAttribute("page", foodService.findAllNotEatenForCurrentHousehold(pageable));
        return "foods";
    }

    @GetMapping("/all")
    public String getAll(Model model, @PageableDefault Pageable pageable) {
        model.addAttribute("page", foodService.findAllForCurrentHousehold(pageable));
        model.addAttribute("all", true);

        return "foods";
    }

    @GetMapping("/eaten")
    public String getAllEaten(Model model, @PageableDefault Pageable pageable) {
        model.addAttribute("page", foodService.findAllEatenForCurrentHousehold(pageable));
        model.addAttribute("eaten", true);
        return "foods";
    }

    @GetMapping("/add")
    public String showAddForm(Model model) {
        AddFoodDto newFoodDto = new AddFoodDto();
        List<FoodType> availableFoodTypes = foodTypeService.findAllForCurrentHousehold();

        LocalDate defaultExpires = LocalDate.now().plusDays(availableFoodTypes.isEmpty() ? 7 : availableFoodTypes.get(0).getDefaultDurability());

        // default values
        newFoodDto.setCreated(LocalDate.now());
        newFoodDto.setExpires(defaultExpires);
        newFoodDto.setQuantity(1);
        newFoodDto.setNotificationPeriod(3);

        model.addAttribute("food", newFoodDto);
        model.addAttribute("types", availableFoodTypes);
        model.addAttribute("names", foodService.getUniqueNames());
        model.addAttribute("placements", Placement.values());
        return "add-food";
    }

    @PostMapping("/add")
    public String create(@Valid @ModelAttribute("food") AddFoodDto newFoodDto, BindingResult result, Model model, @PageableDefault Pageable pageable) {
        if (result.hasErrors()) {
            model.addAttribute("food", newFoodDto);
            model.addAttribute("types", foodTypeService.findAllForCurrentHousehold());
            model.addAttribute("placements", Placement.values());
            return "add-food";
        }

        Food newFood = newFoodDto.toFood();
        newFood.setEaten(0);

        foodService.create(newFood);
        model.addAttribute("page", foodService.findAllNotEatenForCurrentHousehold(pageable));
        return "redirect:/foods";
    }

    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
        Food food = foodService.findById(id);

        model.addAttribute("types", foodTypeService.findAllForCurrentHousehold());
        model.addAttribute("food", food);
        model.addAttribute("names", foodService.getUniqueNames());
        model.addAttribute("placements", Placement.values());

        return "update-food";
    }

    @PostMapping("/edit/{id}")
    public String update(@PathVariable("id") long id, @Valid Food food, BindingResult result, Model model, @PageableDefault Pageable pageable) {
        if (result.hasErrors()) {
            food.setId(id);
            model.addAttribute("types", foodTypeService.findAllForCurrentHousehold());
            model.addAttribute("food", food);
            model.addAttribute("placements", Placement.values());
            return "update-food";
        }

        foodService.update(food);
        model.addAttribute("page", foodService.findAllNotEatenForCurrentHousehold(pageable));
        return "redirect:/foods";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") long id, Model model, @RequestHeader(value = "referer") String referer, @PageableDefault Pageable pageable) {
        Food food = foodService.findById(id);

        foodService.delete(food);
        model.addAttribute("page", foodService.findAllNotEatenForCurrentHousehold(pageable));
        return "redirect:" + referer;
    }

    @Transactional
    @GetMapping("/eat/{id}")
    public String eat(@PathVariable("id") long id, @RequestParam(name = "all", required = false) boolean all, Model model, @PageableDefault Pageable pageable) {
        Food food = foodService.findById(id);

        if (all) {
            food.setEaten(food.getQuantity());
        } else {
            food.setEaten(food.getEaten() + 1);
        }
        foodService.update(food);

        model.addAttribute("page", foodService.findAllNotEatenForCurrentHousehold(pageable));
        return "redirect:/foods";
    }

    @Transactional
    @GetMapping("/uneat/{id}")
    public String uneat(@PathVariable("id") long id, Model model, @PageableDefault Pageable pageable) {
        Food food = foodService.findById(id);

        food.setEaten(food.getEaten() - 1);
        foodService.update(food);
        model.addAttribute("page", foodService.findAllEatenForCurrentHousehold(pageable));
        return "redirect:/foods/eaten";
    }
}
