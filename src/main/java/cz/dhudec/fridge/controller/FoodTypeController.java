package cz.dhudec.fridge.controller;

import cz.dhudec.fridge.domain.FoodType;
import cz.dhudec.fridge.service.HouseholdHolder;
import cz.dhudec.fridge.service.api.FoodTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import javax.validation.Valid;
import java.time.LocalDate;

@Controller
@RequestMapping(value = "/foodTypes")
@SessionAttributes("foodType")
public class FoodTypeController {

    private final FoodTypeService foodTypeService;
    private final HouseholdHolder householdHolder;

    @Autowired
    public FoodTypeController(FoodTypeService foodTypeService, HouseholdHolder householdHolder) {
        this.foodTypeService = foodTypeService;
        this.householdHolder = householdHolder;
    }

    @GetMapping("")
    public String getAll(Model model) {
        model.addAttribute("foodTypes", foodTypeService.findAllForCurrentHousehold());
        return "food-types";
    }

    @GetMapping("/add")
    public String showAddForm(Model model) {
        FoodType newFoodType = new FoodType();
        newFoodType.setCreated(LocalDate.now());
        newFoodType.setHousehold(householdHolder.get());

        model.addAttribute("foodType", newFoodType);
        model.addAttribute("types", foodTypeService.findAllForCurrentHousehold());
        return "add-food-type";
    }

    @PostMapping("/add")
    public String create(@Valid @ModelAttribute("foodType") FoodType newFoodType, BindingResult result, Model model, SessionStatus status) {
        if (result.hasErrors()) {
            model.addAttribute("foodType", newFoodType);
            model.addAttribute("types", foodTypeService.findAllForCurrentHousehold());
            return "add-food-type";
        }

        status.setComplete();
        foodTypeService.create(newFoodType);
        model.addAttribute("foodTypes", foodTypeService.findAllForCurrentHousehold());
        return "redirect:/foodTypes";
    }

    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
        FoodType foodType = foodTypeService.findById(id);

        model.addAttribute("types", foodTypeService.findAllForCurrentHousehold());
        model.addAttribute("foodType", foodType);

        return "update-food-type";
    }

    @PostMapping("/edit/{id}")
    public String update(@PathVariable("id") long id, @Valid FoodType foodType, BindingResult result, Model model) {
        if (result.hasErrors()) {
            foodType.setId(id);
            return "update-food-type";
        }

        foodTypeService.update(foodType);
        model.addAttribute("foodTypes", foodTypeService.findAllForCurrentHousehold());
        return "redirect:/foodTypes";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") long id, Model model, @RequestHeader(value = "referer") String referer) {
        FoodType foodType = foodTypeService.findById(id);

        foodTypeService.delete(foodType);
        model.addAttribute("types", foodTypeService.findAllForCurrentHousehold());
        return "redirect:" + referer;
    }
}
