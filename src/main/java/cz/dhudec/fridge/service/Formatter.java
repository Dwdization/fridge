package cz.dhudec.fridge.service;

import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;

@Service
public final class Formatter {

    public String   formatFoodExpiration(LocalDate expiryDate) {
        LocalDate today = LocalDate.now();
        long daysDifference = today.getLong(ChronoField.EPOCH_DAY) - expiryDate.getLong(ChronoField.EPOCH_DAY);

        // today
        if (daysDifference == 0) {
            return "today";
        }

        // in the past
        if (daysDifference > 0) {
            if (daysDifference == 1) {
                return "yesterday";
            } else {
                return daysDifference + " days ago";
            }
        }

        // in the future
        if (daysDifference == -1) {
            return "tomorrow";
        } else {
            if (daysDifference >= -5) {
                return "in " + -daysDifference + " days";
            } else {
                return "on " + expiryDate.format(DateTimeFormatter.ofPattern("dd. MMM yyyy"));
            }
        }
    }
}
