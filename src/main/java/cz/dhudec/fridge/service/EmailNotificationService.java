package cz.dhudec.fridge.service;

import cz.dhudec.fridge.service.api.NotificationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
@Slf4j
public class EmailNotificationService implements NotificationService {

    private final JavaMailSender mailSender;

    public EmailNotificationService(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    @Override
    public void sendSimpleNotification(Notification notification) {
        SimpleMailMessage message = new SimpleMailMessage();

        message.setTo(notification.getReceivers().stream().map(Notification.Addressee::getAddress).toArray(String[]::new));
        message.setSubject(notification.getHeader());
        message.setText(notification.getBody());

        mailSender.send(message);
    }

    @Override
    @Transactional
    public void sendAdvancedNotification(Notification notification) {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, "utf-8");

        if (notification.getReceivers().isEmpty()) {
            log.warn("Attempted to send a notification with no receivers! Subject: {}", notification.getHeader());
            return;
        }

        try {
            helper.setTo(notification.getReceivers().stream().map(Notification.Addressee::getAddress).toArray(String[]::new));
            helper.setSubject(notification.getHeader());

            helper.setText(notification.getBody(), true);
            mailSender.send(mimeMessage);
        } catch (MessagingException e) {
            log.error("Sending a monitoring notification failed!", e);
            e.printStackTrace();
        }
    }
}
