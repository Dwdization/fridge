package cz.dhudec.fridge.service;

import cz.dhudec.fridge.domain.Household;
import cz.dhudec.fridge.service.api.HouseholdService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.function.Supplier;

@Service
public class HouseholdHolder implements Supplier<Household> {

    private final HouseholdService householdService;

    public HouseholdHolder(HouseholdService householdService) {
        this.householdService = householdService;
    }

    @Override
    public Household get() {
        String login = SecurityContextHolder.getContext().getAuthentication().getName();

        return householdService.findByLogin(login);
    }
}
