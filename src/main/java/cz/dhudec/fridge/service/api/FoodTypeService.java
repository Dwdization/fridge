package cz.dhudec.fridge.service.api;

import cz.dhudec.fridge.domain.FoodType;
import org.springframework.lang.Nullable;

import java.util.List;

public interface FoodTypeService {

    List<FoodType> findAllForCurrentHousehold();

    FoodType create(final FoodType food);

    FoodType update(FoodType food);

    void delete(FoodType food);

    boolean hasAnyFood(FoodType foodType);

    @Nullable
    FoodType findById(Long id);
}
