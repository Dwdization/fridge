package cz.dhudec.fridge.service.api;

import cz.dhudec.fridge.domain.Household;

public interface HouseholdService {

    Household findByLogin(String login);
}
