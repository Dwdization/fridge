package cz.dhudec.fridge.service.api;

import lombok.Data;

import java.util.Set;

public interface NotificationService {
    void sendSimpleNotification(Notification notification);

    void sendAdvancedNotification(Notification notification);

    @Data
    class Notification {
        private String header;
        private String body;
        private Set<? extends Addressee> receivers;

        public interface Addressee {
            String getAddress();
        }
    }
}

