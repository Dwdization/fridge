package cz.dhudec.fridge.service.api;

import cz.dhudec.fridge.domain.Food;
import cz.dhudec.fridge.domain.FoodType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface FoodService {

    List<Food> findAllForCurrentHousehold();

    Page<Food> findAllForCurrentHousehold(Pageable pageable);

    List<Food> findAllNotEatenForCurrentHousehold();

    Page<Food> findAllNotEatenForCurrentHousehold(Pageable pageable);

    List<Food> findAllEatenForCurrentHousehold();

    Page<Food> findAllEatenForCurrentHousehold(Pageable pageable);

    boolean existsByType(FoodType foodType);

    Food create(final Food food);

    Food update(Food food);

    void delete(Food food);

    Food findById(Long id);

    List<String> getUniqueNames();
}
