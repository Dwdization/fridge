package cz.dhudec.fridge.service;

import cz.dhudec.fridge.domain.Food;
import cz.dhudec.fridge.domain.FoodType;
import cz.dhudec.fridge.domain.Household;
import cz.dhudec.fridge.repository.FoodRepository;
import cz.dhudec.fridge.service.api.FoodService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FoodServiceJpa implements FoodService {

    private final FoodRepository foodRepository;
    private final HouseholdHolder householdHolder;

    public FoodServiceJpa(FoodRepository foodRepository, HouseholdHolder householdHolder) {
        this.foodRepository = foodRepository;
        this.householdHolder = householdHolder;
    }

    @Override
    public List<Food> findAllForCurrentHousehold() {
        Household currentHousehold = householdHolder.get();
        return foodRepository.findAllForHousehold(currentHousehold);
    }

    @Override
    public Page<Food> findAllForCurrentHousehold(Pageable pageable) {
        Household currentHousehold = householdHolder.get();
        return foodRepository.findAllForHousehold(currentHousehold, pageable);
    }

    @Override
    public List<Food> findAllNotEatenForCurrentHousehold() {
        Household currentHousehold = householdHolder.get();
        return foodRepository.findAllByEatenForHousehold(currentHousehold, false);
    }

    @Override
    public Page<Food> findAllNotEatenForCurrentHousehold(Pageable pageable) {
        Household currentHousehold = householdHolder.get();
        return foodRepository.findAllByEatenForHousehold(currentHousehold, false, pageable);
    }

    @Override
    public List<Food> findAllEatenForCurrentHousehold() {
        Household currentHousehold = householdHolder.get();
        return foodRepository.findAllByEatenForHousehold(currentHousehold, true);
    }

    @Override
    public Page<Food> findAllEatenForCurrentHousehold(Pageable pageable) {
        Household currentHousehold = householdHolder.get();
        return foodRepository.findAllByEatenForHousehold(currentHousehold, true, pageable);
    }

    @Override
    public boolean existsByType(FoodType foodType) {
        return foodRepository.existsByType(foodType);
    }

    @Override
    public Food create(Food food) {
        return foodRepository.save(food);
    }

    @Override
    public Food update(Food food) {
        return foodRepository.save(food);
    }

    @Override
    public void delete(Food food) {
        foodRepository.delete(food);
    }

    @Override
    public Food findById(Long id) {
        return foodRepository
                .findById(id)
                .orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public List<String> getUniqueNames() {
        return foodRepository.getUniqueNames();
    }
}
