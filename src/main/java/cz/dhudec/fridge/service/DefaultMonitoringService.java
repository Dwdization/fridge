package cz.dhudec.fridge.service;

import cz.dhudec.fridge.repository.HouseholdRepository;
import cz.dhudec.fridge.service.api.NotificationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class DefaultMonitoringService {
    private final HouseholdRepository householdRepository;
    private final NotificationCreator notificationCreator;
    private final NotificationService notificationService;

    public DefaultMonitoringService(HouseholdRepository householdRepository, NotificationCreator notificationCreator, NotificationService notificationService) {
        this.householdRepository = householdRepository;
        this.notificationCreator = notificationCreator;
        this.notificationService = notificationService;
    }

    // TODO(D. Hudec): 19.8.19 udělat to tak, že plánování nebude v kódu, ale v cronasaservice službě, která přistoupí na budící url a hned dojde k odeslání

    // every day at 16:00:00
    @Scheduled(cron = "0 0 14 * * *")
    public void sendNotificationForExpiringFoods() {
        householdRepository.findAll()
                .stream()
                .peek(household -> log.info("Sending notification for {}.", household.getName()))
                .map(notificationCreator)
                .forEach(notificationService::sendAdvancedNotification);
    }
}
