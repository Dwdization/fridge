package cz.dhudec.fridge.service;

import cz.dhudec.fridge.domain.FoodType;
import cz.dhudec.fridge.domain.Household;
import cz.dhudec.fridge.repository.FoodTypeRepository;
import cz.dhudec.fridge.service.api.FoodService;
import cz.dhudec.fridge.service.api.FoodTypeService;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FoodTypeServiceJpa implements FoodTypeService {

    private final FoodTypeRepository foodTypeRepository;
    private final FoodService foodService;
    private final HouseholdHolder householdHolder;

    public FoodTypeServiceJpa(FoodTypeRepository foodTypeRepository, FoodService foodService, HouseholdHolder householdHolder) {
        this.foodTypeRepository = foodTypeRepository;
        this.foodService = foodService;
        this.householdHolder = householdHolder;
    }

    @Override
    public List<FoodType> findAllForCurrentHousehold() {
        Household currentHousehold = householdHolder.get();
        return foodTypeRepository.findAllByHouseholdOrderById(currentHousehold);
    }

    @Override
    public FoodType create(FoodType foodType) {
        return foodTypeRepository.save(foodType);
    }

    @Override
    public FoodType update(FoodType foodType) {
        return foodTypeRepository.save(foodType);
    }

    @Override
    public void delete(FoodType foodType) {
        if (hasAnyFood(foodType)) {
            throw new UnsupportedOperationException("FoodTypes with any foods under them cannot be deleted!");
        }

        foodTypeRepository.delete(foodType);
    }

    @Override
    public boolean hasAnyFood(FoodType foodType) {
        return foodService.existsByType(foodType);
    }

    @Override
    @Nullable
    public FoodType findById(Long id) {
        return foodTypeRepository
                .findById(id)
                .orElseThrow(IllegalArgumentException::new);
    }
}
