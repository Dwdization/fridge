package cz.dhudec.fridge.service;

import cz.dhudec.fridge.domain.Household;
import cz.dhudec.fridge.dto.FoodNotificationDto;
import cz.dhudec.fridge.repository.FoodRepository;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.expression.ThymeleafEvaluationContext;

import java.util.List;
import java.util.Locale;
import java.util.function.Function;
import java.util.stream.Collectors;

import static cz.dhudec.fridge.service.api.NotificationService.Notification;

@Service
public class NotificationCreator implements Function<Household, Notification> {

    private final TemplateEngine templateEngine;
    private final FoodRepository foodRepository;
    private final ApplicationContext applicationContext;

    public NotificationCreator(TemplateEngine templateEngine, FoodRepository foodRepository, ApplicationContext applicationContext) {
        this.templateEngine = templateEngine;
        this.foodRepository = foodRepository;
        this.applicationContext = applicationContext;
    }

    @Override
    public Notification apply(Household household) {
        Notification notification = new Notification();
        notification.setHeader(String.format("Fridge status (%s)", household.getName()));
        notification.setReceivers(household.getEmails());

        List<FoodNotificationDto> foodToExpireDtos = foodRepository.findFoodsThatNeedNotificationForHousehold(household)
                .stream()
                .map(FoodNotificationDto::from)
                .collect(Collectors.toList());

        Context thContext = new Context(Locale.getDefault());
        thContext.setVariable("foods", foodToExpireDtos);
        thContext.setVariable(
                ThymeleafEvaluationContext.THYMELEAF_EVALUATION_CONTEXT_CONTEXT_VARIABLE_NAME,
                new ThymeleafEvaluationContext(applicationContext, null)
        );

        String mailContent = templateEngine.process("emails/expiring-foods-notification.html", thContext);

        notification.setBody(mailContent);

        return notification;
    }
}
