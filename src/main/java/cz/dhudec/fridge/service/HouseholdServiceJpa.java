package cz.dhudec.fridge.service;

import cz.dhudec.fridge.domain.Household;
import cz.dhudec.fridge.repository.HouseholdRepository;
import cz.dhudec.fridge.service.api.HouseholdService;
import org.springframework.stereotype.Service;

@Service
public class HouseholdServiceJpa implements HouseholdService {

    private final HouseholdRepository householdRepository;

    public HouseholdServiceJpa(HouseholdRepository householdRepository) {
        this.householdRepository = householdRepository;
    }

    @Override
    public Household findByLogin(String login) {
        return householdRepository.findOneByLogin(login);
    }
}
